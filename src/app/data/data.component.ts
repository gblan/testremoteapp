import { Component, OnInit } from '@angular/core';
import {MessageService} from "primeng/api";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  accessToken?: String;
  toastName?: string;

  constructor(private messageService: MessageService,
              private httpClient: HttpClient) { }

  ngOnInit(): void {
    if (history.state && history.state.accessToken) {
      this.accessToken = history.state.accessToken;
    }
    if (history.state && history.state.toastName) {
      this.toastName = history.state.toastName;
    }

    this.httpClient.get('https://httpbin.org/get').subscribe();

    this.messageService.add({
      key: this.toastName,
      severity: 'info',
      summary: 'Loaded',
      detail: 'The DataComponent has loaded.'
    });
  }

}
