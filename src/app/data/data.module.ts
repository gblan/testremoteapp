import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataComponent } from './data.component';
import {DataRoutingModule} from "./data.routes";

@NgModule({
  declarations: [
    DataComponent
  ],
  exports: [
    DataComponent
  ],
  imports: [
    CommonModule,
    DataRoutingModule
  ]
})
export class DataModule { }
