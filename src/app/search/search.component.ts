import { Component, OnInit } from '@angular/core';
import {MessageService} from "primeng/api";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  accessToken?: String;
  toastName?: string;

  constructor(private messageService: MessageService,
              private httpClient: HttpClient) { }

  ngOnInit(): void {
    if (history.state && history.state.accessToken) {
      this.accessToken = history.state.accessToken;
    }
    if (history.state && history.state.toastName) {
      this.toastName = history.state.toastName;
    }

    this.httpClient.get('https://httpbin.org/get').subscribe();

    this.messageService.add({
      key: this.toastName,
      severity: 'info',
      summary: 'Loaded',
      detail: 'The SearchComponent has loaded.'
    });
  }

}
